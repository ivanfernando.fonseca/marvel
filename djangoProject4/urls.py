from django.contrib import admin
from django.urls import path
from characters import views as views_characters
from users import views as users_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('list/', views_characters.list, name='marvel_characters'),
    path('savelist', views_characters.save_list),
    path('users/login/', users_views.login_v,   name='login'),
    path('users/logout/', users_views.logout_v, name='logout'),
    path('users/signup/', users_views.signup_v, name='signup'),
    path('users/profile', users_views.profile,  name='profile'),
    path('characters/create', views_characters.create2,  name='create_character'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
