from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from characters.models import Character
from characters.forms import CharacterForm
from django.shortcuts import render, redirect

charactersList=[
    { 'name': '1THANOS',
      'description': 'Using the power of the Infinity Stones, Thanos believes he can ultimately save    the universe by wiping out half of its  population',
      'image': 'https://terrigen-cdn-dev.marvel.com/content/prod/1x/019tha_ons_crd_03.jpg'
     },
    { 'name': '2BLACK PANTHER',
      'description': 'Challa is the king of the secretive and highly advanced African nation of Wakanda - as well as the powerful warrior known as the Black Panther',
      'image': 'https://terrigen-cdn-dev.marvel.com/content/prod/1x/007blp_ons_crd_02.jpg'
     },
    { 'name': '3IRON MAN',
      'description': 'Genius. Billionaire. Philanthropist. Tony Starks confidence is only matched by his high - flying abilities as the hero called *Iron Man.',
      'image': 'https://terrigen-cdn-dev.marvel.com/content/prod/1x/002irm_ons_crd_03.jpg'
    },
    { 'name': '4BLACK WIDOW',
      'description': 'Despite super spy Natasha Romanoff’s checkered past, she’s become one of S.H.I.E.L.D.’s most deadly assassins and a frequent member of the Avengers.',
      'image': 'https://terrigen-cdn-dev.marvel.com/content/prod/1x/011blw_ons_crd_03.jpg'
     },
]

@login_required
def list(request):
    list=Character.objects.all()
    return render(request,'marvel/index.html', {'characters':list})


def save_list(request):
    for diccionarioCharacter in charactersList:
        Character.objects.create(name = diccionarioCharacter['name'],
                                  description = diccionarioCharacter['description'],
                                  image = diccionarioCharacter['image'])
    return HttpResponse("se ha almacenado la informacion con exito  ")


def getChacter(request, id):
    characterBD=Character.objects.get(id=id)
    return HttpResponse('el personaje seleccionado es {}'.format(characterBD.name))

def get_by_power(request,powerParameter):
    list=Character.objects.filter(power__startswith=powerParameter)
    return  render(request,'marvel/index.html', {'characters':list})

def get_by_name(request,nameParameter):
    list=Character.objects.filter(name__contains=nameParameter)
    return  render(request,'marvel/index.html', {'characters':list})

def  create(request,nameParameter,descriptionParameter):
    characterModel=Character()
    characterModel.name=nameParameter
    characterModel.description=descriptionParameter
    characterModel.save()
    return HttpResponse('ok')

@login_required()
def create2(request):
    if request.method == 'POST':
        form = CharacterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('marvel_characters')
        else:
            return render(request,'marvel/create.html', {'form': form})

    return render(request, 'marvel/create.html ')
