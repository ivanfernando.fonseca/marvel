from django.contrib import admin
from users.models import Profile

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('pk','user','phone_number','website','picture')
    list_display_links = ('phone_number', 'website')

    search_fields = (
        'user_email',
        'user_first_name'
    )

