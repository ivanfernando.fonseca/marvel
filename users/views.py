from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from users.models import Profile
from users.forms import ProfileForm


def login_v(request):
    if  request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request,username=username,password=password)
        if user:
            login(request, user)
            return redirect('marvel_characters')
        else:
            return render(request, 'users/login.html', {'error': 'invalid username or password'})
    return render(request, 'users/login.html')

@login_required
def logout_v(request):
    logout(request)
    return redirect('login')


def signup_v(request):
    if request.method=='GET':
        return render(request, 'users/signup.html')

    username=request.POST['username']
    password=request.POST['password']
    password2=request.POST['password2']
    first_name=request.POST['first_name']
    last_name=request.POST['last_name']
    email=request.POST['email']
    if password2 != password:
        return render(request,'users/signup.html',{'error':'password does not match'})

    user = User.objects.create_user(username=username,password=password)
    user.first_name = first_name
    user.last_name = last_name
    user.email = email
    user.save()
    profile = Profile(user=user)
    profile.save()
    return redirect('login')


@login_required
def profile(request):
    profile = request.user.profile

    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data
            profile.website = data['website']
            profile.phone_number = data['phone_number']
            profile.picture = data['picture']
            print(data)
            profile.save()
            return redirect('profile')

    else:
        form = ProfileForm()

    return render(
        request=request,
        template_name='users/profile.html',
        context={
            'profile': profile,
            'user': request.user,
            'form': form
        }
    )


